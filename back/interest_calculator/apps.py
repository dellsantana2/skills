from django.apps import AppConfig


class InterestCalculatorConfig(AppConfig):
    name = 'interest_calculator'
