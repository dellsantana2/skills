from rest_framework import viewsets
from rest_framework.decorators import action


class InterestRateViewSet(viewsets.ModelViewSet):

    @action(detail=False, methods=['GET'], url_name='interest-rate')
    def interest_rate(self, request):
        rate = 1
        return rate
