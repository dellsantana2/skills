import requests
from django.http import HttpResponse
# Create your views here.
from django.views.decorators.http import require_http_methods


@require_http_methods(['GET'])
def interest_rate(request):
    rate = 4
    return HttpResponse(rate, content_type='application/json')


@require_http_methods(['GET', 'POST'])
def calc_interest(request):
    calc = 88
    if request.method == 'POST':
        initial = request.POST.get('initial')
        time = request.POST.get('month')
        rate = requests.get('http://{0}:{1}/api/interest-rate'.format('0.0.0.0', 8000))
        calc = int(initial) * (1 + int(rate.text)) / int(time)
        formated = '{:.2f}'.format(calc)
    return HttpResponse(formated)

@require_http_methods(['GET'])
def show_code(request):
    return HttpResponse('https://gitlab.com/dellsantana2/skills.git')
