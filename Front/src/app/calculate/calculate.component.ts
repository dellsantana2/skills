import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-calculate',
  templateUrl: './calculate.component.html',
  styleUrls: ['./calculate.component.scss']
})
export class CalculateComponent implements OnInit {

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.getGitrubUrl();
  }

  getGitrubUrl() {
    this.http.get('http://127.0.0.1:8000/api/show-me-your-code').subscribe(result => {
      console.log(result);
    });
  }
}
